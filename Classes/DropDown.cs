using System;
using System.ComponentModel.Design;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace ProjetoNUnit
{
    [TestFixture]
    public class DropDown
    {
        [Test]
         public void ManipularCombobox()
         {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/dropdown");
            driver.FindElement(By.Id("dropdown")).Click();
            new SelectElement(driver.FindElement(By.Id("dropdown"))).SelectByText("Option 2");
            driver.FindElement(By.Id("page-footer")).Click();
            driver.Quit();
         }

         [Test]
         public void ValidarCasoDeTeste()
         {
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("http://blackmirror.crowdtest.me.s3-website-us-east-1.amazonaws.com/auth");
            driver.FindElement(By.Id("login")).Click();
            driver.FindElement(By.Id("login")).SendKeys("marcelo.silva@base2.com.br");
            driver.FindElement(By.Id("password")).Click();
            driver.FindElement(By.Id("password")).SendKeys("#HenriquE1#");
            driver.FindElement(By.XPath("//a[@aria-label='dismiss cookie message']")).Click();
            //driver.FindElement(By.XPath("//button[@type='submit']")).Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//button[@type='submit']"))).Click();
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//button[text()='Gerenciar']"))).Click();
            //driver.FindElement(By.ClassName("li-projects ng-star-inserted")).Click();
            driver.FindElement(By.XPath("//li[4]/a/i")).Click();
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//mat-cell[@class='mat-cell cdk-column-identifier mat-column-identifier ng-star-inserted']"))).Click();
            driver.FindElement(By.Id("mat-tab-label-0-2")).Click();
            string projeto = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//*[text()='Cadastrar Release']"))).Text;
            Assert.AreEqual("Cadastrar Release", projeto);
            driver.Quit();
            
         }
    }

}