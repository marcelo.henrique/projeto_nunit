using System.ComponentModel.Design;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using System.Threading;

namespace ProjetoNUnit
{
    [TestFixture]
    public class FindElements 
    {

     [Test]
        public void PesquisaGoogle()
        
        {
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://google.com.br");
            driver.FindElement(By.Name("q")).SendKeys("selenium");
            Thread.Sleep(1000);
            driver.FindElement(By.XPath("//div[5]//center//input[1]")).Click();
            Thread.Sleep(1000);
            Assert.AreEqual("selenium - Pesquisa Google", driver.Title);
            driver.Quit();

        }
     [Test]
        public void ValidarFormulario()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://ultimateqa.com/filling-out-forms/");
            driver.FindElement(By.Id("et_pb_contact_name_0")).SendKeys("Marcelo");
            driver.FindElement(By.Id("et_pb_contact_message_0")).SendKeys("Testando!");
            Thread.Sleep(1000);
            driver.FindElement(By.XPath("(//button[@type='submit'])[1]")).Click();
            Thread.Sleep(2000);
            string aviso = driver.FindElement(By.XPath("//p[text()='Thanks for contacting us']")).Text;
            Assert.AreEqual("Thanks for contacting us", aviso);
            driver.Quit();

        }

     [Test]
        public void ValidaFormularioApenasUmCampo()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://ultimateqa.com/filling-out-forms/");
            driver.FindElement(By.Id("et_pb_contact_name_0")).SendKeys("Marcelo");
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("//input[@name='et_pb_contactform_submit_0']/parent::form//button[@type='submit']")).Click();
            string alerta = driver.FindElement(By.XPath("//p[text()='Please, fill in the following fields:']")).Text;
            Assert.AreEqual("Please, fill in the following fields:", alerta);
            driver.Quit();
        }

     [Test]
        public void ValidarLogin()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://conexaoqa.herokuapp.com/");
            driver.FindElement(By.XPath("//a[@data-test='landing-login']")).Click();
            driver.FindElement(By.Name("email")).Click();
            driver.FindElement(By.Name("email")).SendKeys("marcelo.silva@base2.com.br");
            driver.FindElement(By.Name("password")).Click();
            driver.FindElement(By.Name("password")).SendKeys("teste123");
            driver.FindElement(By.XPath("//input[@value='Login']")).Click();
            Thread.Sleep(1000);
            string posLogin = driver.FindElement(By.XPath("//p[@data-test='dashboard-welcome']")).Text;
            Thread.Sleep(2000);
            Assert.AreEqual("Bem-vindo Marcelo", posLogin);
            driver.Quit();
        }

     }
} 