using System.ComponentModel.Design;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace ProjetoNUnit
{            

    [TestFixture]
    public class AcessarPaginaWeb
    {

        [Test]
        public void navegarParaOSiteDoGoogle()
            {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://google.com.br");

            driver.Quit();
            }

         [Test]
         public void navegarParaOSiteDoGoogle1()
            {
            IWebDriver driver = new ChromeDriver();
	        driver.Navigate().GoToUrl("https://google.com.br");
            Assert.AreEqual("Google", driver.Title);

	        driver.Quit();
            }

        [Test]
        public void navegarParaOSiteDoGoogle2()
            {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://google.com.br");
            Assert.AreEqual("https://www.google.com.br/", driver.Url);

             driver.Quit();
            }       
    }

}
