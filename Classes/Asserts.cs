using System.Data;
using System.ComponentModel.Design;
using System;
using NUnit.Framework;

namespace ProjetoNUnit
{

[TestFixture]

    public class Asserts
    {

        [Test]
        public void VerificarDadosLivro()

        {
            string livro = "Livro1";
            Double valor = 35.99;

            Assert.IsTrue(livro.Equals("Livro1"));
            Assert.IsTrue(valor.Equals(35.99));
        }

        [Test]
        public void VerificarStrings()

        {
            string var1 = "Marcelo";
            string var2 = "marcelo";

            Assert.IsFalse(var1.Equals(var2));
        }


        [Test] 
        public void VerificarNome()

        {
            string var1 = "Marcelo";

            Assert.AreEqual("Marcelo", var1);
        }


    }
}